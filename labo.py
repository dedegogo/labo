#!/usr/bin/python3

import json
import csv


class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass


class AbsentException(LaboException):
    pass


class PresentException(LaboException):
    pass


'''gestion labo'''


def labo(fichier):
    '''
    Un labo est un dict composé des bureaux (value) et des noms (key)
    '''
    try:
        with open(fichier, mode="r") as file:
            return json.load(file)
            print(json.load(file))
    except:
        return {}


def arrivee_labo(labo, nom_membre, bureau):  #: str, no_bureau: str
    ''' ajouter une personne au labo et lui assigner un bureau
        on doit aussi lever une exception si la persone est deja dans le labo
    '''
    if nom_membre in labo:
        raise PresentException
    labo[nom_membre] = bureau


def depart_labo(labo, nom_membre):
    '''Enlever une personne du labo et liberer le bureau,
    exception si personne ! dans labo'''
    if nom_membre not in labo:
        raise
    del labo[nom_membre]


def mod_bureau(labo, nom_membre, nouveau_bureau):
    if nom_membre not in labo:
        raise AbsentException
    labo[nom_membre] = nouveau_bureau


def chg_nom(labo, nom_membre, nouveau_nom):
    if nom_membre not in labo:
        raise AbsentException
    if nouveau_nom in labo:
        raise PresentException
    labo[nouveau_nom] = (labo[nom_membre])
    del labo[nom_membre]


def au_labo(labo, nom_membre):
    if nom_membre in labo:
        return True


def quel_bureau(labo, nom_membre):
    if nom_membre not in labo:
        raise AbsentException

    return nom_membre, labo[nom_membre]


def liste_nom_bureau(labo):
    if labo is None:
        raise LaboException
    for key, value in labo.items():
        print(f"{key} est au bureau {value}", end=', ')
        print("")


def liste_bureau_ocu(labo):
    if labo is None:
        raise LaboException
    labo_bureau = {}
    for key, value in labo.items():
        labo_bureau.setdefault(value, []).append(key)
    for key in sorted(labo_bureau.keys()):
        print(key)
        for value in sorted(labo_bureau[key]):
            print(f"-{value}")
    return labo_bureau


def bureau_ocu_html(labo, fichier):
    if labo is None:
        raise LaboException
    labo = liste_bureau_ocu(labo)
    with open(fichier, mode="w") as file:
        file.write("<html>")
        file.write("<head>")
        file.write("<title>Occupation des bueraux</title>")
        file.write("</head>")
        file.write("<body>")
        file.write("<h1>Occupation des bureaux</h1> \n")
        for key in sorted(labo.keys()):
            file.write(f"<h3>{key}</h3>")
            for value in sorted(labo[key]):
                file.write(f"<p>- {value}</p>")
            file.write("<br>")
        file.write("</body>")
        file.write("</html>")


def save_labo(labo, fichier):
    with open(fichier, mode="w") as file:
        file.write(json.dumps(labo))


def import_csv(labo, fichier):
    with open(fichier, newline='', encoding='utf-8') as fichier_csv:
        fichier_temp = csv.DictReader(fichier_csv)
        for line in fichier_temp:
            nom = line['Nom'].strip().title()
            bureau = line['Bureau'].strip().title()
            if nom not in labo:
                labo[nom] = bureau
            else:
                if bureau != labo[nom]:
                    print(line['Nom'], line['Bureau'])
