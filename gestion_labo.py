from labo import *
from menu import *


def gestion_labo():
    ''''
    gestion_labo doit gerer plusieurs elements :


        -création du menu
        -creation de l'objet labo
        
        -excecution du menu
    '''
    # creation des fonctions appelees dans le menu
    def cmd_arrive():
        try:
            nom = input("Nom : ")
            bureau = input("Bureau : ")
            arrivee_labo(labo_1, nom, bureau)
            print(f"{nom} ton bureau est le {bureau}")
        except PresentException:
            print(f"{nom} est deja dans le labo")

    def cmd_depart():
        try:
            nom = input("Nom : ")
            depart_labo(labo_1, nom)
        except AbsentException:
            print(f"{nom} n'est pas dans le labo")

    def cmd_mod_bureau():
        try:
            nom = input("Nom : ")
            nouveau_bureau = input("Nouveau bureau : ")
            mod_bureau(labo_1, nom, nouveau_bureau)
            print (f"le nouveau bureau de {nom} est le {nouveau_bureau}")
        except AbsentException:
            print(f"{nom} n'est pas dans le labo")

    def cmd_chg_nom():
        try:
            nom = input("Nom actuel : ")
            nouveau_nom = input ("Nouveau nom : ")
            chg_nom(labo_1, nom, nouveau_nom)
            print(f"Ton nom à bien été changé de {nom} à {nouveau_nom}")
        except AbsentException:
            print(f"{nom} n'est pas dans le labo")
        except PresentException:
            print(f"{nom} est deja dans le labo")

    def cmd_au_labo():
        nom = input("Nom de la personne recherchée : ")
        if au_labo(labo_1, nom):
            print(f"{nom} est bien au labo")
        else:
            print(f"{nom} ne fait pas partie du labo")

    def cmd_quel_bureau():
        try:
            nom = input("Nom : ")
            nom_membre, bureau = quel_bureau(labo_1, nom)
            print(f"Le bureau de {nom_membre} est le {bureau}")
        except AbsentException:
            print(f"la personne n'est pas dans le labo")

    def cmd_liste_nom_bureau():
        try:
            liste_nom_bureau(labo_1)
        except LaboException:
            print ("il n'y a personne dans ce labo !!")

    def cmd_liste_bureau_ocu():
        try:
            liste_nom_bureau(labo_1)
        except LaboException:
            print("il n'y a personne dans ce labo !!")

    def cmd_bureau_ocu_html():
        try:
            bureau_ocu_html(labo_1, "./ocupation_bureaux.html")
        except LaboException:
            print("il n'y a personne dans ce labo !!")

    def cmd_save_labo():
        save_labo(labo_1, "./personel.json")

    def cmd_import_csv():
        fichier ="./test.csv"
        import_csv(labo_1, fichier)

    # creation du menu
    menu_labo = menu()
    ajouter_element(menu_labo, "Enregistrer arivée", cmd_arrive)
    ajouter_element(menu_labo, "Enregistrer depart", cmd_depart)
    ajouter_element(menu_labo, "Modifier Buerau", cmd_mod_bureau)
    ajouter_element(menu_labo, "Changer nom", cmd_chg_nom)
    ajouter_element(menu_labo, "Verifier apartenance au labo", cmd_au_labo)
    ajouter_element(menu_labo, "Trouver le bureau de quelqu'un", cmd_quel_bureau)
    ajouter_element(menu_labo, "Lister les membres du labo avec leur bureaux", cmd_liste_nom_bureau)
    ajouter_element(menu_labo, "Lister l'ocupation des bureaux", cmd_liste_bureau_ocu)
    ajouter_element(menu_labo, "Lister l'ocupation des bureaux dans une page HTML", cmd_bureau_ocu_html)
    ajouter_element(menu_labo, "Enregister le fichier", cmd_save_labo)
    ajouter_element(menu_labo, "Importer une base de donnes CSV", cmd_import_csv)
    # creation du labo et de la liste des bureaux vides

    labo_1=labo("./personel.json")
    # excecution du menu
    gerer_menu(menu_labo)
    cmd_save_labo()


def main():
    gestion_labo()


main()
