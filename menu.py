
def menu():
    ''' initialise un liste qui va stocker le menu
    in : m = []
    '''
    m = []
    return m


def ajouter_element(m, intitule, commande):
    m.append((intitule, commande))


def _afficher_menu(m):
    for num, (intitule, _) in enumerate(m, 1):
        print(num, intitule)
    print(0, 'Quitter')


def _input_choix(m):
    choix = -1
    while choix not in range(0, (len(m)+1)):
        try:
            choix = int(input(f"Que voulez vous faire ? Entrez un numéro entre 0 et {len(m)} : "))
        except ValueError:
            print(f"ce n'etait pas un nombre entre 0 et {len(m)}")
    return choix


def _traiter_choix(m, choix):
    assert 0 <= choix <= len(m)
    if choix != 0:
        return m[choix-1][1]()
    print("Au revoir")


def gerer_menu(m):
    # gerer l'excecution du menu, faire un while loop
    choix = -1
    while choix != 0:
        _afficher_menu(m)
        choix = _input_choix(m)
        _traiter_choix(m, choix)
        if choix != 0:
            input("tapez enter pour returner au menu")
